%option noyywrap 

%{
#include <string>
#include <iostream>
#include <cstring>
#include "y.tab.h"
int yyparse();


void name_convertion(char* name);
%}

%%
"PRINT" return PRINT;
"IF" return IF;
"WHILE" return WHILE;

[A-Za-z]+ {
	name_convertion(yytext);
	return ZMIENNA;
}
[0-9]+ {
	yylval.iValue = atoi(yytext);
	return LICZBA;
}
[+\-*^/=<>;] {
	return yytext[0];
	}
[ \t\n] ;
.	{
	return UNK;
}
%%

void name_convertion(char* name){
	std::string str(name);
	yylval.vName = new char [str.length()+1];
	strcpy(yylval.vName, str.c_str());
}

void yyerror(std::string msg){
	std::cerr << "Error: " << msg << "!\n";
}