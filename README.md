# Interpreter wraz z kompilatorem

Interpreter prostego języka programowania, tworzonego w ramacha zajęć na Politechnice Poznańskiej.

## Włączanie

Najpierw należy wykorzystać z programu flex++ i poddać analizie leksykalnej plik lek.l.

```bash
flex.exe lek.l
```

Nastepnie analizie semantycznej poddajemy plik bis.y (zakładam, że bison dodany jest do ścieżki środowiskowej).

```bash
bison -dy bis.y
```

W wyniku działania powyższych programów powstały między innymi pliki lex.yy.c i y.tab.c, które należy poddać kompilacji do 
czego wykorzystałem kompilatorMinGW (G++).

```bash
g++ lex.yy.c y.tab.c
```

Na koniec włączamy finalny program, a jako parametr podajemy plik wejściowy (input.txt).

```bash
a.exe < input.txt
```

W skrypcie powłoki test.bat zapisane są wszystkie te komendy.

## INSTRUKCJA

1. Zmienne:
	- Dodanie nowej zmiennej:
		- a=10; 
		- b=a;
	- Działania matematyczne:
		- a=2+4;
		- b=a*10;
		- c=100-301;
		- d=b/10; //zmienne przymują tylko wartości całkowite
2. Instrukcje warunkowe:
	- IF S A:
		- IF -> słowo kluczowe
		- S -> warunek (>, <)
		- A -> przypisanie wykonywane, kiedy warunek zwraca wartość true
	- IF S I:
		- IF -> słowo kluczowe
		- S -> warunek (>, <)
		- I -> instrukcja wykonywana, kiedy warunek zwraca wartość true
3. Petla:
	- WHILE V VI I:
		- V -> zmienna; pętla wykonuje się kiedy V jest różne od 0
		- VI -> działanie nad V
		- I -> działanie do wykonania pewna ilość razy
4. Konsola:
	- PRINT V:
		- V -> zmienna; wyświetlenie jej wartości w konsoli

## KOMPILATOR

Prosty kompilator zamieniający kod z polecenia do kodu Asseblero-podobnego (zbliżonego do kodu poznanego w ramach zajęć ASK na mikroprocesor 8086).