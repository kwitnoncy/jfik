%{
	#include <iostream>
	#include <stdio.h>
	#include <string>
	#include <cstring>
	#include <map>
	#include <stdlib.h>
	#include <vector>
	#include <fstream>
	void yyerror(std::string msg);
	int yylex();

	int wolny_indeks = 8000;
	enum TYP{
		DODAWANIE,
		ODEJMOWANIE,
		MNOZENIE,
		DZIELENIE,
		WIEKSZE,
		MNIEJSZE,
		WYSWIETL,
		BLAD,
	};

	struct Operacja{
		TYP typ;
		int arg1;
		int arg2;
		bool flag;
		std::string zmienna;

		Operacja(){
			typ = BLAD;
			arg1 = 0;
			arg2 = 0;
			flag = false;
			zmienna = "";
		}
		Operacja(TYP typ, int arg1, int arg2){
			this->typ = typ;
			this->arg1 = arg1;
			this->arg2 = arg2;
			this->flag = false;
			zmienna = "";
		}
		Operacja(TYP typ, std::string zmienna){
			this->typ = typ;
			this->arg1 = 0;
			this->arg2 = 0;
			this->flag = false;
			this->zmienna = zmienna;
		}
	};

	int dzialaj(Operacja operacja);
	
	std::vector<Operacja> operacje;

	int dzialaj_ost();
	int dzialaj_przed_ost();
	void zmien_para_ost(int arg);
	void zmien_para_przed_ost(int arg);

	std::ofstream output;
	int if_counter = 1;
	int while_counter = 1;
	std::map<std::string, int> zmienne;
	std::map<std::string, int> zmienne_indeks;
	bool flag_instruction = false;
	bool flag_if = false;
%}

%union {
    int iValue;
	char* vName;
}

%start PROGRAM
%token <iValue> LICZBA
%type <iValue> E
%type <vName> ZMIENNA
%type <iValue> INSTRUKCJA
%token UNK PRINT ZMIENNA WHILE IF

%%

PROGRAM: PROGRAM INSTRUKCJA ';' {dzialaj_ost();}
		| PROGRAM PRINT ZMIENNA ';'{
			output<<"MOV AX, ["<< zmienne_indeks[$3] <<"]\n";
			output<<"INT 21\n";
			std::cout<<$3<<" = "<<zmienne[$3]<<"\n";
		}
  		| PROGRAM ZMIENNA '=' E ';' {
			if(zmienne.find($2)!=zmienne.end()){
				std::cout<<"zmienna " << $2 << " istnieje, przypisano do niej wartosc " << $4 << "\n";
				zmienne[$2] = $4;
				if(flag_instruction){
					output<<"MOV ["<<zmienne_indeks[$2]<<"] AX\n";
					flag_instruction = false;
				}
				else
					output<<"MOV ["<<zmienne_indeks[$2]<<"] "<<$4<<"\n";
			}
			else{
				std::cout<<"zmienna " << $2 << " istnieje, przypisano do niej wartosc " << $4 << "\n";
				zmienne.insert(std::make_pair($2,$4));
				zmienne_indeks.insert(std::make_pair($2,wolny_indeks));
				if(flag_instruction){
					output<<"MOV ["<<zmienne_indeks[$2]<<"] AX\n";
					flag_instruction = false;
				}
				else
					output<<"MOV ["<<zmienne_indeks[$2]<<"] "<<$4<<"\n";
				wolny_indeks++;
			}
		}
		| PROGRAM IF INSTRUKCJA ZMIENNA '=' E ';'{
			if (dzialaj_ost()){
				if(zmienne.find($4)!=zmienne.end()){
					std::cout<<"zmienna " << $4 << " istnieje, przypisano do niej wartosc " << $6 << "\n"; 
					zmienne[$4] = $6;
				}
				else{
					std::cout<<"zmienna " << $4 << " nie istnieje, utworzono ja i przypisano do niej wartosc " << $6 << "\n";
					zmienne.insert(std::make_pair($4,$6));
					zmienne_indeks.insert(std::make_pair($4,wolny_indeks));
					wolny_indeks++;
				}
			}
			output<<"is_true"<<if_counter<<":"<<"\n";
			output<<"\tMOV ["<<zmienne_indeks[$4]<<"] "<<$6<<"\n";
			output<<"is_false"<<if_counter<<":"<<"\n";
			if_counter++;
		}
		| PROGRAM IF INSTRUKCJA ZMIENNA '=' INSTRUKCJA ';'{
			flag_if = true;
			
			int temp = dzialaj_przed_ost();
			
			output<<"is_true"<<if_counter<<":"<<"\n";
			
			int temp1 = dzialaj_ost();
			
			if (temp){
				if(zmienne.find($4)!=zmienne.end()){
					std::cout<<"zmienna " << $4 << " istnieje, przypisano do niej wartosc " << temp1 << "\n"; 
					zmienne[$4] = temp1;
				}
				else{
					std::cout<<"zmienna " << $4 << " nie istnieje, utworzono ja i przypisano do niej wartosc " << temp << "\n";
					zmienne.insert(std::make_pair($4,temp1));
				}
			}
			output<<"is_false"<<if_counter<<":"<<"\n";
			flag_if = false;
		}
		| PROGRAM WHILE ZMIENNA ZMIENNA '=' INSTRUKCJA ZMIENNA '=' INSTRUKCJA ';'{
			output<<"MOV AX "<< operacje[operacje.size()-1].arg1 <<"\n";
			output<<"MOV BX "<< operacje[operacje.size()-1].arg1 <<"\n";
			output<<"MOV CX ["<<zmienne_indeks[$3]<<"]\n";
			output<<"WHILE"<<while_counter<<":\n";
			
			if(operacje[operacje.size()-1].typ == DODAWANIE )
				output<<"\tADD AX BX\n";
			else if(operacje[operacje.size()-1].typ == ODEJMOWANIE )
				output<<"\tSUB AX BX\n";
			else if(operacje[operacje.size()-1].typ == MNOZENIE )
				output<<"\tMUL AX BX\n";
			else if(operacje[operacje.size()-1].typ == DZIELENIE )
				output<<"\tDIV AX BX\n";

			if(operacje[operacje.size()-2].typ == DODAWANIE )
				output<<"\tADD CX "<<operacje[operacje.size()-2].arg2<<"\n";
			else if(operacje[operacje.size()-2].typ == ODEJMOWANIE )
				output<<"\tSUB CX "<<operacje[operacje.size()-2].arg2<<"\n";
			else if(operacje[operacje.size()-2].typ == MNOZENIE )
				output<<"\tMUL CX "<<operacje[operacje.size()-2].arg2<<"\n";
			else if(operacje[operacje.size()-2].typ == DZIELENIE )
				output<<"\tDIV CX "<<operacje[operacje.size()-2].arg2<<"\n";

			output<<"\tJNZ WHILE"<<while_counter<<"\n";
			while(zmienne[$3] != 0){
				int temp1 = dzialaj_ost();
				int temp2 = dzialaj_przed_ost();

				zmien_para_ost(temp1);
				zmien_para_przed_ost(temp2);

				zmienne[$3] = temp2;
				zmienne[$7] = temp1;
			}
			while_counter++;
		}
		| PROGRAM WHILE ZMIENNA ZMIENNA '=' INSTRUKCJA PRINT ZMIENNA ';'{
			output<<"MOV CX ["<<zmienne_indeks[$3]<<"]\n";
			output<<"WHILE"<<while_counter<<":\n";

			output<<"\tMOV AX, ["<< zmienne_indeks[$8] <<"]\n";
			output<<"\tINT 21\n";

			if(operacje[operacje.size()-2].typ == DODAWANIE )
				output<<"\tADD CX "<<operacje[operacje.size()-2].arg2<<"\n";
			else if(operacje[operacje.size()-2].typ == ODEJMOWANIE )
				output<<"\tSUB CX "<<operacje[operacje.size()-2].arg2<<"\n";
			else if(operacje[operacje.size()-2].typ == MNOZENIE )
				output<<"\tMUL CX "<<operacje[operacje.size()-2].arg2<<"\n";
			else if(operacje[operacje.size()-2].typ == DZIELENIE )
				output<<"\tDIV CX "<<operacje[operacje.size()-2].arg2<<"\n";

			output<<"\tJNZ WHILE"<<while_counter<<"\n";
			while(zmienne[$3] != 0){
				int temp2 = dzialaj_ost();
				zmien_para_ost(temp2);
				zmienne[$3] = temp2;
				
				std::cout<<zmienne[$8]<<"\n";
			}
			while_counter++;
		}
		| /*nic*/
  ;

INSTRUKCJA:  E '+' E {
			Operacja temp(DODAWANIE, $1, $3);
			operacje.push_back(temp);
		}
		| E '-' E {
			Operacja temp(ODEJMOWANIE, $1, $3);
			operacje.push_back(temp);
		}
		| E '*' E {
			Operacja temp(MNOZENIE, $1, $3);
			operacje.push_back(temp);
		}
		| E '/' E {
			Operacja temp(DZIELENIE, $1, $3);
			operacje.push_back(temp);
		}
		| E '>' E {
			Operacja temp(WIEKSZE, $1, $3);
			operacje.push_back(temp);

			output<<"MOV AX "<<$1<<"\n";
			output<<"MOV BX "<<$3<<"\n";
			output<<"CMP AX BX\n";
			output<<"JG is_true"<<if_counter<<"\n";
			output<<"JLE is_false"<<if_counter<<"\n";
		}
		| E '<' E {
			Operacja temp(MNIEJSZE, $1, $3);
			operacje.push_back(temp);

			output<<"MOV AX "<<$1<<"\n";
			output<<"MOV BX "<<$3<<"\n";
			output<<"CMP AX BX\n";
			output<<"JL is_true"<<if_counter<<"\n";
			output<<"JGE is_false"<<if_counter<<"\n";
		}
		| PRINT ZMIENNA{
			Operacja temp(WYSWIETL, $2);
			operacje.push_back(temp);
		}
		;

E: LICZBA	{$$ = $1;}
  | ZMIENNA {
	$$ = zmienne[$1];
	printf("wartosc zmiennej %s to %d \n", $1, zmienne[$1]);
  } 
  | E '+' E {
	Operacja temp(DODAWANIE, $1, $3);
	operacje.push_back(temp);
	$$ = dzialaj(operacje[operacje.size()-1]);
	
	output<<"MOV AX "<<$1<<"\n";
	output<<"MOV BX "<<$3<<"\n";
	output<<"ADD AX BX\n";
  	
	flag_instruction = true;
  }
  | E '-' E {
	Operacja temp(ODEJMOWANIE, $1, $3);
	operacje.push_back(temp);
	$$ = dzialaj(operacje[operacje.size()-1]);
	
	output<<"MOV AX "<<$1<<"\n";
	output<<"MOV BX "<<$3<<"\n";
	output<<"SUB AX BX\n";
	flag_instruction = true;
  }
  | E '*' E {
	Operacja temp(MNOZENIE, $1, $3);
	operacje.push_back(temp);
	$$ = dzialaj(operacje[operacje.size()-1]);
	
	output<<"MOV AX "<<$1<<"\n";
	output<<"MOV BX "<<$3<<"\n";
	output<<"MUL AX BX\n";
	flag_instruction = true;
  }
  | E '/' E {
	Operacja temp(DZIELENIE, $1, $3);
	operacje.push_back(temp);
	$$ = dzialaj(operacje[operacje.size()-1]);
	
	output<<"MOV AX "<<$1<<"\n";
	output<<"MOV BX "<<$3<<"\n";
	output<<"DIV AX BX\n";
	flag_instruction = true;
  }
  | E '>' E {
	Operacja temp(WIEKSZE, $1, $3);
	operacje.push_back(temp);
	$$ = dzialaj(operacje[operacje.size()-1]);
  }
  | E '<' E {
	Operacja temp(MNIEJSZE, $1, $3);
	operacje.push_back(temp);
	$$ = dzialaj(operacje[operacje.size()-1]);
  }
  ;
%%
int main(){
	output.open("output.txt");
	yyparse();
	output.close();
}
void yyerror(char* str){
	printf("%s",str);
}
int yywrap(){
	return 0;
}
int dzialaj_ost(){
	return dzialaj(operacje[operacje.size()-1]);
}
int dzialaj_przed_ost(){
	return dzialaj(operacje[operacje.size()-2]);
}
int dzialaj(Operacja operacja){
	int temp;
	
	switch(operacja.typ){
		case DODAWANIE:
			temp = operacja.arg1 + operacja.arg2;
			std::cout << operacja.arg1 << " + " << operacja.arg2 << " = " << temp << "\n";
			break;
		case ODEJMOWANIE:
			temp = operacja.arg1 - operacja.arg2;
			std::cout << operacja.arg1 << " - " << operacja.arg2 << " = " << temp << "\n";
			break;
		case MNOZENIE:
			temp = operacja.arg1 * operacja.arg2;
			std::cout << operacja.arg1 << " * " << operacja.arg2 << " = " << temp << "\n";
			break;
		case DZIELENIE:
			temp = operacja.arg1 / operacja.arg2;
			std::cout << operacja.arg1 << " / " << operacja.arg2 << " = " << temp << "\n";
			break;
		case WIEKSZE:
			if(operacja.arg1 > operacja.arg2)
				temp = 1;
			else
				temp = 0;
			std::cout << operacja.arg1 << " > " << operacja.arg2 << " => " << temp << "\n";
			break;
		case MNIEJSZE:
			if(operacja.arg1 < operacja.arg2)
				temp = 1;
			else
				temp = 0;
			std::cout << operacja.arg1 << " < " << operacja.arg2 << " => " << temp << "\n";
			break;
		case WYSWIETL:
			std::cout<<operacja.zmienna <<" = "<<zmienne[operacja.zmienna]<<"\n";
	}
	return temp;
}
void zmien_para_ost(int arg){
	operacje[operacje.size()-1].arg1 = arg;
}
void zmien_para_przed_ost(int arg){
	operacje[operacje.size()-2].arg1 = arg;
}